import Index from "../pages/Index.vue";
import Award from "../pages/Award.vue";
import User from "../pages/User.vue";
import NotFound from "../pages/404.vue";


const routes = [
    { path: "/", component: Index },
    { path: "/award", component: Award },
    { path: "/user", component: User },
    { path: '/:catchAll(.*)', component: NotFound },
]
export default routes